import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
	apiKey: "AIzaSyCSNv303hHaP3gXjivDN_Ctm1cq11lRA_c",
	authDomain: "instagram-clone-3552d.firebaseapp.com",
	projectId: "instagram-clone-3552d",
	storageBucket: "instagram-clone-3552d.appspot.com",
	messagingSenderId: "190686968254",
	appId: "1:190686968254:web:a8b4499417f553237861ef",
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const storage = getStorage();

export { app, db, storage };
